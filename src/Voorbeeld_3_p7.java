public class Voorbeeld_3_p7 {

	public static void main(String[] args) {
		int getal, tel = 0, nummer, tot = 0;
		double gem;
		int[] aantal = new int[10];
		nummer = Invoer.leesInt("Geef een getal in: ");
		while (nummer != 0) {
			if (nummer > 0 && nummer < 11) {
				aantal[nummer - 1] = aantal[nummer - 1] + 1;
				tel += 1;
				tot += nummer;
			}
			nummer = Invoer.leesInt("Geef een getal: ");
		}
		gem = (double) tot / tel;
		System.out.println("Getal:\tAantal:\n");
		for (int i = 0; i <= 9; i++) {
			System.out.println(i + 1 + "\t" + aantal[i]);
		}
		System.out.println("\nGemiddelde :" + gem);

	}

}
