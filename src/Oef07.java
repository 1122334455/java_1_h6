public class Oef07 {

	public static void main(String[] args) {
		String naam;
		naam = Invoer.leesString("Geef je naam in: ");
		while (!naam.equals("\\")) {
			int positie = naam.indexOf(" ");
			String jaar = Invoer.leesString("Geef het geboortejaar: ");
			int nieuwJaar = Integer.parseInt(jaar.substring(2));
			String teken = bepaalTeken(nieuwJaar);
			String nieuweNaam = Character.toUpperCase(naam.substring(0, positie).charAt(0)) + "." + naam.substring(positie).toUpperCase();
			System.out.println(nieuweNaam + " " + teken);
			naam = Invoer.leesString("Geef je naam in: ");
		}

	}

	private static String bepaalTeken(int nieuwJaar) {
		String[] jaren = { "Rat", "Buffel", "Tijger", "Kat", "Draak", "Slang",
				"Paard", "Geit", "Aap", "Haan", "Hond", "Everzwijn" };
		int index = nieuwJaar % 12;
		String teken = jaren[index];
		return teken;
	}

}
