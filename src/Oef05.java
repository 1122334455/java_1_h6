/*
 * In een bepaalde provincie werd een onderzoek verricht in verband met de wijze waarop men zich naar het werk verplaatst.
 * We geven de keuze van een aantal personen in, deze stopt wanneer er 0 wordt ingegeven.
 * Code-> 1: te voet, 2: fiets, 3: motorfiets of motor, 4: auto, 5: tram, 6: bus, 7: trein, 8: andere
 * Per categorie wil men het aantal personen kennen, ook percentueel tov het totaal aantal personen.
 * Druk per code de transportwijze af (geen code), het aantal personen en het procentueel aandeel.
 * Voor Java: Gebruik H6_oefening5.txt
 */

import java.io.*;

public class Oef05 {

	public static void main(String[] args) throws IOException {
		BestandsLezer a = new BestandsLezer ("H6_oefening5.txt");
		int tot = 0, keuze;
		int [] aantal = new int [8];
		double [] proc = new double [8];
		String [] omschr = {"Te voet", "Fiets", "Motorfiets of motor", "Auto", "Tram", "Bus", "Trein", "Andere" };
		for (int i = 0; i <= 7; i ++) {
			aantal[i] = 0;
		}
		keuze = a.leesInt();
		while (keuze != 0) {
			aantal[keuze - 1] = aantal[keuze - 1] + 1;
			tot += 1;
			keuze = a.leesInt();
		}
		System.out.format("%-30s%-15s%-10s\n\n", "Omschrijving", "Aantal", "Percentueel");
		for (int i = 0; i <= 7; i++) {
			proc[i] = Math.round((double) aantal[i] / tot * 100 * 100) / 100;
			System.out.format("%-30s%-15s%-10s\n", omschr[i], aantal[i], proc[i]);
		}
	}

}
