/*
 * Creeer twee tabellen a en b die elk 20 waarden moeten kunnen bevatten. Ken aan a(i) twee maal de waarde van zijn index toe en aan b(i) drie maal
 * de waarde van zijn index. Kan aan een derde tabel c het product toe van een element uit tabel a met een element uit de omgekeerde tabel b.
 * Druk de waarden af.
 */
public class Oef03 {

	public static void main(String[] args) {
		int [] a = new int[20];
		int [] b = new int[20];
		int [] c = new int[20];
		System.out.println("Index\tA\tB\tC\n");
		for (int i = 0; i <= 19; i++) {
			a[i] = i * 2;
			b[19 -i] = (19 -i) * 3;
			c[i] = a[i] * b[19 - i];
		}
		for (int i = 0; i <= 19; i++) {
			System.out.println(i + "\t" + a[i] + "\t" + b[i] + "\t" + c[i]);
		}

	}

}
