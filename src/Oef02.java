/* Een tabel a bestaat uit 500 gehele getallen. Bereken het rekenkundig gemiddelde van die getallen. 
 * Het getal 0 mag niet meegerekend worden bij de het gemiddelde. De tabel zit reeds in het intern geheugen.
 * Voor Java: Maag gebruik van het bestand H6_oefening2.txt
 */

import java.io.*;

public class Oef02 {

	public static void main(String[] args) throws IOException {
		BestandsLezer lees = new BestandsLezer("H6_oefening2.txt");
		int som = 0, aantal;
		double gemiddelde;
		int[] a = new int[500];
		aantal = a.length;
		for (int i = 0; i <= a.length - 1; i++) {
			a[i] = lees.leesInt();
			if (a[i] != 0) {
				som += a[i];
			} else {
				aantal -= 1;
			}
		}
		gemiddelde = (double) som / aantal;
		System.out.println(gemiddelde);
	}

}
