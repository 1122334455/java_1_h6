public class Voorbeeld_2_p7 {

	public static void main(String[] args) {
		int[] getal = new int[15];
		int kleinst = 0;
		for (int i = 0; i < getal.length; i++) {
			getal[i] = Invoer.leesInt("Geef een getal in:");
		}
		kleinst = getal[0];
		for (int i = 1; i < getal.length; i++) {
			if (getal[i] < kleinst) {
				kleinst = getal[i];
			}
		}
		System.out.println("Het kleinste getal is: " + kleinst);
		System.out.println("Dit getal komt voor op positie(s): ");
		for (int i = 0; i < getal.length; i++) {
			if (getal[i] == kleinst) {
				System.out.println(i);
			}
		}
	}

}
