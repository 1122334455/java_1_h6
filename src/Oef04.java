/* Een firma wil maandelijks de verkoopscijfers van 30 vertegenwoordigers vergelijken. We geven via het toetsenbord in:
 * Vertegenwoordigersnummer, verkoopscijfer en de afwijking tov het gemiddelde (absoluut, 2 decimalen).
 *  
 */
import java.io.*;

public class Oef04 {

	public static void main(String[] args) throws IOException {
		BestandsLezer a = new BestandsLezer("H6_oefening4.txt");
		int som = 0;
		double gem, afw;
		int [] vertNummer = new int[30];
		int [] verkBedrag = new int[30];
		double [] gemVerkCijfer = new double[30];
		for (int i = 0; i <= 29; i++) {
			vertNummer[i] = a.leesInt();
			verkBedrag[i] = a.leesInt();
			som += verkBedrag[i];
		}
		gem = Math.round(som / 30.0 * 100) / 100.0;
		for (int i = 0; i <= 29; i++) {
			afw = gem - verkBedrag[i];
			gemVerkCijfer[i] = Math.abs(afw);
			System.out.println(vertNummer[i] + "\t\t" + verkBedrag[i] + "\t\t" + gemVerkCijfer[i]);
		}
	}

}
