import java.io.*;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class Oef08 {
	static char code;
	static int positie;

	public static void main(String[] args) throws IOException {
		BestandsLezer a = new BestandsLezer("H6_oefening8.txt");
		int[] aantal = new int[5];
		double[] proc = new double[5];
		char[] codes = { 'A', 'B', 'C', 'D', 'E' };
		String naam, gebDatum;
		int hDag, hMaand, hJaar;
		char bs, kar = 'A';
		int klInk, leeftijd, tot = 0, positie2 = 0;
		double grootst;
		GregorianCalendar gc = new GregorianCalendar();
		hDag = gc.get(Calendar.DAY_OF_MONTH);
		hMaand = gc.get(Calendar.MONTH);
		hJaar = gc.get(Calendar.YEAR);
		naam = a.leesRegel();
		while (!(naam.equals("xx") || naam.equals("XX"))) {
			gebDatum = a.leesRegel();
			bs = a.leesRegel().charAt(0);
			klInk = a.leesInt();
			leeftijd = berekenLeeftijd(gebDatum, hDag, hMaand, hJaar);
			bepaalCode(leeftijd, bs, klInk);
			tot += 1;
			aantal[positie] += 1;
			naam = a.leesRegel();
		}
		for (int i = 0; i <= 4; i++) {
			proc[i] = (double) aantal[i] / tot * 100;
			System.out.println("Het procentueel aandeel van code " + kar++
					+ " is: " + proc[i]);
		}
		grootst = proc[0];
		for (int i = 0; i <= 4; i++) {
			if (proc[i] > grootst) {
				grootst = proc[i];
			}
		}
		for (int i = 0; i <= 4; i++) {
			if (proc[i] == grootst) {
				positie2 = i;
			}
		}
		System.out.println("\nDe code die het meest voorkomt is: "
				+ (codes[positie2]));
	}

	private static int berekenLeeftijd(String gebDatum, int hDag,
			int hMaand, int hJaar) {
		int gDag, gMaand, gJaar, hDagI, hMaandI, hJaarI, leeftijd;
		gDag = Integer.parseInt(gebDatum.substring(0, 2));
		gMaand = Integer.parseInt(gebDatum.substring(3, 5));
		gJaar = Integer.parseInt(gebDatum.substring(6, 10));
		leeftijd = hJaar - gJaar;
		if (hMaand < gMaand) {
			leeftijd -= 1;
		} else if (gMaand == hMaand) {
			if (hDag < gDag) {
				leeftijd -= 1;
			}
		}
		return leeftijd;
	}

	private static void bepaalCode(int leeftijd, char bs, int klInk) {
		if (klInk > 50000) {
			code = 'E';
			positie = 4;
		} else if (leeftijd < 30) {
			if (bs == 'O') {
				code = 'A';
				positie = 0;
			} else {
				code = 'B';
				positie = 1;
			}
		} else if (leeftijd < 50) {
			code = 'C';
			positie = 2;
		} else {
			code = 'D';
			positie = 3;
		}

	}

}
