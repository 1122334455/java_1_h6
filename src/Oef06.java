/*
 * Bij de dagelijkse schriftelijke opname van de aanwezigheden wordt het nummer van de afwezigen opgeschreven. Alle nummers worden ingegeven (0 om te eindigen.
 * De nummers van alle leerlingen die meer dan 2 maal afwezig waren worden afgedrukt, het aantal studenten wordt op voorhand ingegeven.
 * De nummers varieren van 1 tot het aantal studenten in de klas.
 */

public class Oef06 {

	public static void main(String[] args) {
		int aantStud, sN;
		aantStud = Invoer.leesInt("Geef het aantal studenten in: ");
		int[] afW = new int[aantStud + 1];
		sN = Invoer.leesInt("Geef het studentennummer in: ");
		while (sN != 0) {
			afW[sN] += 1;
			sN = Invoer.leesInt("Geef het studentennummer in: ");
		}
		System.out.println("Deze studenten waren meer dan 2 maal afwezig:\n");
		for (int i = 0; i <= aantStud; i++) {
			if (afW[i] > 2) {
				System.out.println("Student " + i);
			}
		}
	}

}
