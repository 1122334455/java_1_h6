public class Voorbeeld_1_p7 {

	public static void main(String[] args) {
		double[] tabel = new double[10];
		double tot = 0, gem = 0;
		for (int i = 0; i <= 9; i++) {
			tabel[i] = Invoer.leesDouble("Geef een getal in:");
			tot += tabel[i];
			gem = tot / tabel.length;
		}
		int tel = 0;
		for (int i = 0; i <= 9; i++) {
			if (tabel[i] == gem) {
				tel += 1;
			}
		}
		System.out.println(tel);

	}

}
